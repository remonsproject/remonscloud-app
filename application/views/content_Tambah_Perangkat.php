<!--main content start-->

<section id="main-content">

    <section class="wrapper">

        <div class="row">

            <div class="col-lg-12">

                <h3 class="page-header"><i class="fa fa-comment-o"></i> Pesan</h3>

                <ol class="breadcrumb">

                    <li><i class="fa fa-comment-o"></i> <a href="Pesan"> Pesan</a></li>

                    <li>Tambah Pesan</li>

                </ol>

            </div>

        </div>

        <div class="row">

            <div class="col-md-12">

                <div class="panel panel-default">

                    <div class="panel-heading">

                        <h2><i class="fa fa-plus"></i><strong>Tambah Perangkat</strong></h2>

                    </div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-12">

                                <form class="form-horizontal" method="post" action="Tambah_Perangkat/addDevice" enctype="multipart/form-data">

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Kode Site *</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="device_code" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Kode Lisensi *</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="license_code" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Nama Site *</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="site_name" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Alamat</label>
                                        <div class="col-lg-5">
                                            <textarea name="address" class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Kecamatan</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="area" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Provinsi</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="province" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Zona Waktu</label>
                                        <div class="col-lg-5">
                                            <select class="form-control" class="time_zone" name="time_zone">
                                                <option>Pilih Zona Waktu</option>
                                                <option value="wib" selected="selected">WIB</option>
                                                <option value="wit">WIT</option>
                                                <option value="wita">WITA</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Latitude *</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="latitude" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Longitude *</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="longitude" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Kapasitas Terpasang
                                            (KWp)</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="capacity" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Nomor Mobile Site</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="no_mobile_site" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Kontak Operator</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="contact_operator" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">No Telepon HP</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="phone_number" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Kontraktor Pemasang</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="install_contractor" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Tahun Pasang</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="install_year" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Status</label>
                                        <div class="col-lg-5">
                                            <select class="form-control" class="status" name="status">
                                                <option>Pilih Status</option>
                                                <option selected="selected">Aktif</option>
                                                <option>Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">File Manual Book</label>
                                        <div class="col-lg-5">
                                            <input type="file" id="manual_book" name="manual_book" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Wiring Diagram</label>
                                        <div class="col-lg-5">
                                            <input type="file" id="wiring_diagram" name="wiring_diagram" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Dokumen Survey</label>
                                        <div class="col-lg-5">
                                            <input type="file" id="survey_document" name="survey_document" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-3 col-lg-5">
                                            <button type="submit" class="btn btn-success">Tambah</button>
                                        </div>
                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>



    </section>

</section>