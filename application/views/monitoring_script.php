<script>


var data_hour_energy = {
        labels: <?php echo json_encode($label_hour); ?>,
        datasets: [{
            label: 'Energi',
            data: <?php echo json_encode($energy_data_hourly); ?>,
            backgroundColor: '#ff6384',
            borderColor: '#ff6384',
            borderWidth: 1,
            fill:false,
        }]
}

var data_hour_power = {
    labels: <?php echo json_encode($label_hour); ?>,
    datasets: [{
        label: 'Daya',
        data: <?php echo json_encode($power_data_hourly); ?>,
        backgroundColor: '#36a2eb',
        borderColor: '#36a2eb',
        borderWidth: 1,
        fill:false,
    }]
    
}

var data_day_energy = {
        labels: <?php echo json_encode($label_day); ?>,
        datasets: [{
            label: 'Energi',
            data: <?php echo json_encode($energy_data_daily); ?>,
            backgroundColor: '#ff6384',
            borderColor: '#ff6384',
            borderWidth: 1,
            fill:false,
        }]
}

var data_day_power = {
    labels: <?php echo json_encode($label_day); ?>,
        datasets: [{
            label: 'Daya',
            data: <?php echo json_encode($power_data_daily); ?>,
            backgroundColor: '#36a2eb',
            borderColor: '#36a2eb',
            borderWidth: 1,
            fill:false,
        }]
}

var data_month_energy = {
        labels: <?php echo json_encode($label_month); ?>,
        datasets: [{
            label: 'Energi',
            data: <?php echo json_encode($energy_data_monthly); ?>,
            backgroundColor: '#ff6384',
            borderColor: '#ff6384',
            borderWidth: 1,
            fill:false,
        }]
}

var data_month_power = {
        labels: <?php echo json_encode($label_month); ?>,
        datasets: [{
            label: 'Daya',
            data: <?php echo json_encode($power_data_monthly); ?>,
            backgroundColor: '#36a2eb',
            borderColor: '#36a2eb',
            borderWidth: 1,
            fill:false,
        }]
}

var optionsChart = {
    responsive: true,
    mainAspectRatio : true,
    aspectRatio : 4
}

function start(){

    var ctx_hour_energy = document.getElementById('canvas_hour_energy').getContext('2d');
    var myChart = new Chart(ctx_hour_energy,{
        type: 'line',
        data: data_hour_energy,
        options: optionsChart
    });

    var ctx_hour_power = document.getElementById('canvas_hour_power').getContext('2d');
    var myChart = new Chart(ctx_hour_power,{
        type: 'line',
        data: data_hour_power,
        options: optionsChart
    });

    var ctx_day_energy = document.getElementById('canvas_day_energy').getContext('2d');
    var myChart = new Chart(ctx_day_energy,{
        type: 'line',
        data: data_day_energy,
        options: optionsChart
    });

    var ctx_day = document.getElementById('canvas_day_power').getContext('2d');
    var myChart = new Chart(ctx_day,{
        type: 'line',
        data: data_day_power,
        options: optionsChart
    });

    var ctx_month_energy = document.getElementById('canvas_month_energy').getContext('2d');
    var myChart = new Chart(ctx_month_energy,{
        type: 'line',
        data: data_month_energy,
        options: optionsChart
    });

    var ctx_month_power = document.getElementById('canvas_month_power').getContext('2d');
    var myChart = new Chart(ctx_month_power,{
        type: 'line',
        data: data_month_power,
        options: optionsChart
    });
}

window.onload = start();
</script>